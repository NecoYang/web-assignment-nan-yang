var ICTinformation = [
    {
        title: "Software Developers & Programmers",
        jobs: 590,
        Max: 100,
        Min: 72,
        des: "Software developers and programmers develop and maintain computer software, websites and software applications (apps).",
        skill: [
            "* computer software and systems",
            "* programming languages and techniques",
            "* software development processes such as Agile",
            "* confidentiality, data security and data protection issues"
        ],
        img: "0"
    },
    {
        title: "Database & Systems Administrators",
        jobs: 74,
        Max: 90,
        Min: 66,
        des: "Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures.",
        skill: [
            "* a range of database technologies and operating systems",
            "* new developments in databases and security systems",
            "* computer and database principles and protocols"
        ],
        img: "1"
    },
    {
        title: "Help Desk & IT Support",
        jobs: 143,
        Max: 65,
        Min: 46,
        des: "Information technology (IT) helpdesk/support technicians set up computer and other IT equipment and help prevent, identify and fix problems with IT hardware and software.",
        skill: [
            "* computer hardware, software, networks and websites",
            "* the latest developments in information technology"
        ],
        img: "2"
    },
    {
        title: "Data Analyst",
        jobs: 270,
        Max: 128,
        Min: 69,
        des: "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims.",
        skill: [
            "* data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R",
            "* data analysis, mapping and modelling techniques",
            "* analytical techniques such as data mining"
        ],
        img: "3"
    },
    {
        title: "Test Analyst",
        jobs: 127,
        Max: 98,
        Min: 70,
        des: "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems.",
        skill: [
            "* programming methods and technology",
            "* computer software and systems",
            "* project management"
        ],
        img: "4"
    },
    {
        title: "Project Management",
        jobs: 188,
        Max: 190,
        Min: 110,
        des: "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress.",
        skill: [
            "* principles of project management",
            "* approaches and techniques such as Kanban and continuous testing",
            "* how to handle software development issues",
            "* common web technologies used by the scrum team"
        ],
        img: "5"
    }
];


$(function () {
    Table();
    changeInformation();
    getData();
});

function Table() {
    for (var i = 0; i < ICTinformation.length; i++) {
        var tabletbody = $(".jobs").append("<tr>"
            + "<td>" + ICTinformation[i].title + "</td>"
            + "<td>" + ICTinformation[i].jobs + "</td>"
            + "<td>" + ICTinformation[i].Max + "</td>"
            + "<td>" + ICTinformation[i].Min + "</td>"
            + "</tr>"
        );
    }
}

function changeInformation() {
    $(document).on("click", "tr", function () {
        var title = this.firstChild.textContent;
        for (var j = 0; j < ICTinformation.length; j++) {
            if (title == ICTinformation[j].title) {
                $(".page")[0].src = "../web-assignment/img/" + ICTinformation[j].img + ".jpg";
                $("span")[1].textContent = ICTinformation[j].title;
                $(".page")[2].textContent = ICTinformations[j].des;
                $("dd").hide();
                for (var k = 0; k < ICTinformation[j].skill.length; k++) {
                    var skills = $("dl").append("<dd>" + ICTinformations[j].skill[k] + "</dd>");
                }
            }
        }

    });
}


function getData() {
    num = ICTinformation.length;
    var total = 0;
    var totalMax = 0;
    var totalMin = 0;
    for (var i = 0; i < num; i++) {
        total = total + ICTinformation[i].jobs;
        totalMax = totalMax + ICTinformation[i].Max;
        totalMin = totalMin + ICTinformation[i].Min;
    }
    avgMax = Math.floor(totalMax / num);
    avgMin = Math.floor(totalMin / num);


    $(".getData").append("<tr><td></td>"
        + "<td>Total:" + total + "</td>"
        + "<td>Average:" + avgMax + "</td>"
        + "<td>Average:" + avgMin + "</td>"
        + "</tr>");
    
}



